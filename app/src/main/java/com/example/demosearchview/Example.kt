package com.example.demosearchview

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.util.*
import kotlin.collections.ArrayList


class Example{
    @SerializedName("result")
    @Expose
     val result: ArrayList<Result>?= null
     val idLight : String = UUID.randomUUID().toString()
 }