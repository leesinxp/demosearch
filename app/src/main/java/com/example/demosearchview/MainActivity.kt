package com.example.demosearchview

import android.app.SearchManager
import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.WindowManager
import androidx.appcompat.widget.SearchView
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*
import kotlin.collections.ArrayList

class MainActivity : AppCompatActivity() {
    private val TAG = "MainActivity"
    private val mListUser = ArrayList<Result>()// vl;
    private val mAdapter = UserAdapter()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val searchManager = getSystemService(Context.SEARCH_SERVICE) as SearchManager
        searchView.setSearchableInfo(searchManager.getSearchableInfo(componentName))
        searchView.setIconifiedByDefault(false)
        rcv_search.layoutManager = LinearLayoutManager(this)
        rcv_search.adapter = mAdapter
        rcv_search.setHasFixedSize(false)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
        getDataFromApi()
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                 filter(newText.toString().toLowerCase(Locale.getDefault()))
                return true
            }

        })
    }

    private fun getDataFromApi() {
        ApiService.endPoint.data().enqueue(object : Callback<Example> {
            override fun onResponse(call: Call<Example>, response: Response<Example>) {
                if (response.isSuccessful) {
                   mListUser.addAll(response.body()?.result!!)
                    Log.d("jj",mListUser.size.toString())
//                    printLog("123haha: ${response.body()!![0].username}")

                    }
                mAdapter.submitList(mListUser)
            }

            override fun onFailure(call: Call<Example>, t: Throwable) {
                printLog(t.toString())
            }

        })
    }
//    private fun showData(data : MainModel) {
//        val results = data.result
//        for (result in results) {
//            printLog("title: ${result.title}")
//        }
//        mListUser.addAll(results)
//        mAdapter.submitList(mListUser)
//    }
    private fun printLog(message: String) {
        Log.d(TAG, message)
    }
    private fun filter(keyWord: String){
        var adapterList = ArrayList<Result>()
        Log.d("kk",mListUser.size.toString())
        mListUser.forEach {
            if (it.title?.toLowerCase(Locale.getDefault())?.contains(keyWord.toLowerCase(Locale.getDefault())) == true
                    ){
                adapterList.add(it)
                Log.d("kk1",adapterList.size.toString())
            }
        }
        mAdapter.submitList(adapterList)
    }
}


