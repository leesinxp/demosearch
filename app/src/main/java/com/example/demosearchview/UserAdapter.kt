package com.example.demosearchview

import android.annotation.SuppressLint
import android.util.Log.d
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.demo_search.view.*

class UserAdapter : ListAdapter<Result, ViewHolderUser>(DemoDiffCallBack()){
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolderUser {
        return ViewHolderUser(
            LayoutInflater.from(parent.context).inflate(R.layout.demo_search, parent, false)
        )
    }

    override fun onBindViewHolder(holder: ViewHolderUser, position: Int) {
        holder.binData(getItem(position))
    }
    }
    class ViewHolderUser(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun binData(everything: Result) {
            itemView.run {
                Glide.with(this).load(everything.image).into(image_user)
              tv_user.text = everything.title
                d("haha1","phone: ${everything.title}")
                linear_Layout.setOnClickListener {
                    everything.expandable = !everything.expandable
                    expandable_Layout.visibility = if(everything.expandable) View.GONE else View.VISIBLE
                }
//            }
//        }
//    }
            }
        }
    }

class DemoDiffCallBack : DiffUtil.ItemCallback<Result>() {
    override fun areItemsTheSame(oldItem: Result, newItem: Result): Boolean {
        return oldItem.idLight == newItem.idLight
    }

    @SuppressLint("DiffUtilEquals")
    override fun areContentsTheSame(oldItem: Result, newItem: Result): Boolean {
        return oldItem == newItem
    }
}

