package com.example.demosearchview

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName
import java.util.*


class Result {
    @SerializedName("id")
    @Expose
     val id: Int? = null

    @SerializedName("title")
    @Expose
     val title: String? = null

    @SerializedName("image")
    @Expose
     val image: String? = null
    val idLight : String =UUID.randomUUID().toString()
    var expandable = false
}